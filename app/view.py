from app import app
from flask import render_template


@app.route('/')
def index():
    date = 3.03
    return render_template('index.html', data=date)
